package mysqlitem;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.material.MaterialData;

import java.sql.*;

public class ShopCommand implements CommandExecutor {
	
	public static final String useMsg = ChatColor.RED + "/shop <add/del> [ID:META-ID] [����������]";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!cmd.getName().equalsIgnoreCase("shop"))
			return false;
		
		if(MySQLItem.stat == null)
			return true;
		
		Player p = (Player) sender;
		if(!(args.length == 2 || args.length == 3)) {
			p.sendMessage(useMsg);
			return true;
		}
		if(args[0].equalsIgnoreCase("add")) {
			if(!MySQLItem.cfg.getBoolean("exchange", false))
				return true;
			if(!args[1].contains(":")) {
				p.sendMessage(useMsg);
				return true;
			}
			String[] idam = args[1].split(":");
			int id;
			int metaid;
			int a;
			try {
				id = Integer.parseInt(idam[0]);
				metaid = Integer.parseInt(idam[1]);
				a = Integer.parseInt(idam[2]);
			} catch (Exception e) {
				p.sendMessage(useMsg);
				return true;
			}
			PlayerInventory inv = p.getInventory();
			ItemStack[] ia = inv.getContents();
			boolean b = false;
			for(ItemStack i : ia) {
				if(i.getTypeId() == id && i.getAmount() >= a && i.getData().getData() == metaid) {
					b = true;
				}
			}
			if(!b) {
				p.sendMessage(ChatColor.RED + "� ��� ��� ������ ��������!");
				return true;
			}
			final String insertQuery = "INSERT INTO `table` (`username`, `id`, `meta-id`, `message`) values ('"+p.getName()+"', "+id+", "+metaid+", '�� ����� ����!')";
			Thread t = new Thread(new Runnable() {
				public void run() {
					try {
						MySQLItem.stat.executeUpdate(insertQuery);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			t.start();
			for(int i = 0; i <= a; i++)
				inv.remove(id);
		} else if(args[0].equalsIgnoreCase("del")) {
			int amount = 1;
			
			int id;
			int meta = 1;
			boolean hasMeta = false;
			if(args[1].contains(":"))
				hasMeta = true;
			String[] sp = null;
			if(hasMeta) {
				try {
				sp = args[1].split(":");
				} catch (Exception e) { return true; }
			}
			try {
				if(hasMeta) {
					id = Integer.parseInt(sp[0]);
					meta = Integer.parseInt(sp[1]);
				} else {
					id = Integer.parseInt(args[1]);
				}
				if(args.length == 3)
					amount = Integer.parseInt(args[2]);
			} catch (Exception e) {
				p.sendMessage(useMsg);
				return true;
			}
			PlayerInventory inv = p.getInventory();
			String selectQuery = "SELECT * FROM `" + MySQLItem.cfg.getString("table") +  "` WHERE `username` = '" + p.getName() + "' AND `id` = " + id;
			ResultSet set = null;
			try {
				set = MySQLItem.stat.executeQuery(selectQuery);
			} catch (Exception e) {
				e.printStackTrace();
			}
			boolean done = false;
			try {
				while(set.next()) {
					if(set.getInt("meta_id") != meta)
						continue;
					
					int a = set.getInt("amount");
					
					if(a < amount) {
						continue;
					}
					ItemStack is = new ItemStack(set.getInt("id"), amount, (short) meta);
					inv.addItem(is);
					if(a - amount == 0)
						MySQLItem.stat.executeUpdate("DELETE FROM `" + MySQLItem.cfg.getString("table") + "` WHERE `id_s` = '" + set.getInt("id_s") + "'");
					else
						MySQLItem.stat.executeUpdate("UPDATE `" + MySQLItem.cfg.getString("table") + "` set `amount`='" + (a - amount) + "' WHERE `id_s`= '" + set.getInt("id_s") + "'");
					p.sendMessage(ChatColor.GREEN + "�� ��������, ��� ������!");
					done = true;
					break;
				}
			} catch (Exception e) {
			}
			if(!done) {
				p.sendMessage(ChatColor.RED + "� ��� ��� ����� �������, ���� ������ ���������� �������� � �������!");
			}
		} else {
			p.sendMessage(useMsg);
		}
		return true;
	}

}
