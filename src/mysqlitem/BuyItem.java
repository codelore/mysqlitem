package mysqlitem;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class BuyItem extends JavaPlugin {
	
	Logger log;
	public static Statement stat = null;
	public static File f;
	public static FileConfiguration cfg;
	
	public void onEnable() {
		f = new File(getDataFolder(), "config.yml");
		cfg = YamlConfiguration.loadConfiguration(f);
		ShopCommand cmd = new ShopCommand();
		this.getCommand("shop").setExecutor(cmd);
		
		cfg.set("username", cfg.get("username", "USERNAME"));
		cfg.set("pass", cfg.get("pass", "PASSWORD"));
		cfg.set("address", cfg.get("address", "DATABASE_ADDRESS"));
		cfg.set("db", cfg.get("db", "DATABASE"));
		cfg.set("table", cfg.get("table", "TABLE"));
		try {
			cfg.save(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		connectDb();
	}
	
	public void onDisable() {
		
	}
	
	private void connectDb() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance(); 
	
			String username = cfg.getString("username");;
			String password = cfg.getString("pass");
			String adress = cfg.getString("address");;
			String db = cfg.getString("db");
	
			String dbURL = "jdbc:mysql://" + adress + "/" + db + "?user=" + username + "&password=" + password;
		
			java.sql.Connection myConnection = DriverManager.getConnection(dbURL);
		
		    stat = myConnection.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
			stat = null;
			this.setEnabled(false);
		}
		String checkQ = "CREATE TABLE IF NOT EXISTS `shop` (" +
  "`id` int(11) NOT NULL," +
  "`id_s` int(11) NOT NULL AUTO_INCREMENT," +
  "`username` varchar(255) NOT NULL," +
"`amount` int(11) NOT NULL," +
  "`meta_id` int(11) NOT NULL," +
  "PRIMARY KEY (`id_s`)" +
") ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		try {
			this.stat.executeUpdate(checkQ);
		} catch (Exception e) {}
	}

}
